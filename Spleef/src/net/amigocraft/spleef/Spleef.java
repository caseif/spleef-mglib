package net.amigocraft.spleef;

import java.util.logging.Logger;

import net.amigocraft.mglib.api.ConfigManager;
import net.amigocraft.mglib.api.Stage;
import net.amigocraft.mglib.api.ArenaFactory;
import net.amigocraft.mglib.api.Minigame;
import net.amigocraft.mglib.api.Round;
import net.amigocraft.mglib.event.player.PlayerJoinMinigameRoundEvent;
import net.amigocraft.mglib.event.player.PlayerLeaveMinigameRoundEvent;
import net.amigocraft.mglib.event.round.MinigameRoundEndEvent;
import net.amigocraft.mglib.event.round.MinigameRoundPrepareEvent;
import net.amigocraft.mglib.event.round.MinigameRoundStartEvent;
import net.amigocraft.mglib.event.round.MinigameRoundTickEvent;
import net.amigocraft.mglib.exception.ArenaExistsException;
import net.amigocraft.mglib.exception.ArenaNotExistsException;
import net.amigocraft.mglib.exception.InvalidLocationException;
import net.amigocraft.mglib.exception.PlayerOfflineException;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

public class Spleef extends JavaPlugin implements Listener {

	public JavaPlugin plugin;
	public Logger log;
	public Minigame mg;

	public void onEnable(){
		plugin = this;
		log = getLogger();
		Bukkit.getPluginManager().registerEvents(this, this);
		mg = Minigame.registerPlugin(this); // register plugin with MGLib
		ConfigManager cm = mg.getConfigManager();
		cm.setDefaultPreparationTime(0);
		cm.setDefaultPlayingTime(0);
		cm.setLobbyArenaColor(ChatColor.GOLD);
		cm.setAllowJoinRoundInProgress(false);
		cm.setPlayerClass(SpleefPlayer.class);
		log.info(this + " has been enabled!");
	}

	public void onDisable(){
		log.info(this + " has been disabled!");
		mg = null;
		log = null;
		plugin = null;
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		if (label.equalsIgnoreCase("spleef")){ // command began with /spleef
			if (sender instanceof Player){ // make sure they're an in-game player since all commands use locations
				if (args.length > 0){ // check that there's at least a first argument
					if (args[0].equalsIgnoreCase("arena")){
						if (args.length > 1){
							if (args[1].equalsIgnoreCase("create")){
								if (sender.hasPermission("spleef.arena.create")){
									if (args.length > 2){
										try {
											mg.createArena(args[2],	((Player)sender).getLocation());
											sender.sendMessage(ChatColor.GREEN + "Successfully created arena " + args[2] + "!");
										}
										catch (ArenaExistsException ex){
											sender.sendMessage(ChatColor.RED + "An arena of the same name already exists!");
										}
									}
									else
										sender.sendMessage(ChatColor.RED + "Not enough arguments! Usage: /spleef arena create "
												+ "[arena name]");
								}
								else
									sender.sendMessage(ChatColor.RED + "You do not have permission to perform this command!");
							}
							else if (args[1].equalsIgnoreCase("delete")){
								if (sender.hasPermission("spleef.arena.delete")){
									if (args.length > 2){
										try {
											mg.deleteArena(args[2]);
											sender.sendMessage(ChatColor.GREEN + "Successfully deleted arena " + args[2] + "!");
										}
										catch (ArenaNotExistsException ex){
											sender.sendMessage(ChatColor.RED + "An arena by that name does not exist!");
										}
									}
									else
										sender.sendMessage(ChatColor.RED + "Not enough arguments! Usage: /spleef arena delete "
												+ "[arena name]");
								}
								else
									sender.sendMessage(ChatColor.RED + "You do not have permission to perform this command!");
							}
							else if (args[1].equalsIgnoreCase("join")){
								if (sender.hasPermission("spleef.arena.join")){
									if (args.length > 2){
										try {
											Round r = mg.getRound(args[2]);
											if (r == null)
												r = mg.createRound(args[2]);
											try {
											r.addPlayer(sender.getName());
											sender.sendMessage(ChatColor.GREEN + "Successfully joined round " + args[2] + "!");
											}
											catch (IllegalArgumentException ex){
												if (ex.getMessage().contains("already in arena"))
													sender.sendMessage(ChatColor.RED + "You are already in a round!");
											}
										}
										catch (ArenaNotExistsException ex){
											sender.sendMessage(ChatColor.RED + "An arena by that name does not exist!");
										}
										catch (PlayerOfflineException ex){
											sender.sendMessage(ChatColor.RED + "Error: you do not appear to be online. O.o");
										}
									}
									else
										sender.sendMessage(ChatColor.RED + "Not enough arguments! Usage: /spleef arena join "
												+ "[arena name]");
								}
								else
									sender.sendMessage(ChatColor.RED + "You do not have permission to perform this command!");
							}
							else if (args[1].equalsIgnoreCase("leave")){
								if (sender.hasPermission("spleef.arena.leave")){
									try {
										mg.getMGPlayer(sender.getName()).removeFromRound();
										sender.sendMessage(ChatColor.GREEN + "Successfully left round!");
									}
									catch (Exception ex){
										sender.sendMessage(ChatColor.RED + "You are not currently in a round!");
									}
								}
								else
									sender.sendMessage(ChatColor.RED + "You do not have permission to perform this command!");
							}
						}
						else
							sender.sendMessage(ChatColor.RED + "Not enough arguments! Usage: /spleef arena [command]");
					}
					else if (args[0].equalsIgnoreCase("spawn")){
						if (args.length > 1){
							if (args[1].equalsIgnoreCase("add")){
								if (args.length > 2){
									ArenaFactory af = mg.getArenaFactory(args[2]);
									if (!af.isNewInstance())
										try {
											af.addSpawn(((Player)sender).getLocation());
										}
									catch (InvalidLocationException e){
										sender.sendMessage(ChatColor.RED + "You must be in the same world as the arena!");
									}
									else // arena does not exist, so we should destroy the arena factory
										af.destroy();
								}
								else
									sender.sendMessage(ChatColor.RED + "Not enough arguments! Usage: /spleef spawn add "
											+ "[arena name]");
							}
							else if (args[1].equalsIgnoreCase("remove")){
								if (args.length > 3){
									ArenaFactory af = mg.getArenaFactory(args[2]);
									if (!af.isNewInstance()) // arena exists
										af.deleteSpawn(Integer.parseInt(args[3])); //TODO: Check this
									else // arena does not exist, so we should destroy the arena factory
										af.destroy();
								}
								else
									sender.sendMessage(ChatColor.RED + "Not enough arguments! Usage: /spleef spawn remove "
											+ "[arena name] [spawn index]");
							}
						}
						else
							sender.sendMessage(ChatColor.RED + "Not enough arguments! Usage: /spleef spawn [add/remove]");
					}
				}
				else
					sender.sendMessage(ChatColor.RED + "Not enough arguments! Usage: /spleef [command]");
			}
			else
				sender.sendMessage(ChatColor.RED + "You must be a player to use Spleef commands!");
			return true;
		}
		return false;
	}

	@EventHandler
	public void onRoundTick(MinigameRoundTickEvent e){
		log.info("t: " + e.getRound().getArena() + ", " + e.getRound().getStage() + ", " + e.getRound().getTime());
	}

	@EventHandler
	public void onRoundPrepare(MinigameRoundPrepareEvent e){
		log.info("p: " + e.getRound().getArena() + ", " + e.getRound().getStage() + ", " + e.getRound().getTime());
	}

	@EventHandler
	public void onRoundStart(MinigameRoundStartEvent e){
		log.info("s: " + e.getRound().getArena() + ", " + e.getRound().getStage() + ", " + e.getRound().getTime());
	}

	@EventHandler
	public void onRoundEnd(MinigameRoundEndEvent e){
		log.info("e: " + e.getRound().getArena() + ", " + e.getRound().getStage() + ", " + e.getRound().getTime());
	}

	@EventHandler
	public void onPlayerJoin(PlayerJoinMinigameRoundEvent e){
		log.info("j: " + e.getRound().getArena() + ", " + e.getPlayer().getName());
		if (e.getRound().getStage() == Stage.WAITING)
			e.getRound().start();
	}

	@EventHandler
	public void onPlayerLeave(PlayerLeaveMinigameRoundEvent e){
		if (e.getRound() == null)
			log.info("round is null");
		log.info("l: " + e.getRound()
				.getArena() + ", " +
				e.getPlayer().getName());
	}

}
